import sys, os
import requests, json, wget
import sqlite3
import time, mutagen.mp3, random, threading
from collections import defaultdict

def init():
	from pathlib import Path
	Path("songfiles").mkdir(parents=True, exist_ok=True)
	Path("textfiles").mkdir(parents=True, exist_ok=True)

class Song():
	def __init__(self, song_name, artist, anime, vintage, anime_type, category, song_type_specific, song_type, mp3, song_id, ann_id, state=0, guess_1='', guess_2='', guess_3='', notes='', time_state=0):
		self.song_name = song_name
		self.artist = artist
		self.anime = anime
		self.vintage = vintage
		self.anime_type = anime_type
		self.category = category
		self.song_type_specific = song_type_specific
		self.song_type = song_type
		self.mp3 = mp3
		self.state = state
		self.song_id = song_id
		self.ann_id = ann_id
		self.guess_1 = guess_1
		self.guess_2 = guess_2
		self.guess_3 = guess_3
		self.notes = notes
		self.time_state = time_state

class Database():
	def __init__(self, DBFILE='db.db'):
		self.DBFILE = DBFILE
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		try:
			cur.execute('SELECT state FROM init_check')
		except:
			init()
			
			cur.execute('CREATE TABLE init_check(state)')
			cur.execute('INSERT INTO init_check VALUES(1)')
			cur.execute('CREATE TABLE songs(song_name, artist, anime, vintage, anime_type, category, song_type_specific, song_type, mp3, song_id, ann_id, state, guess_1, guess_2, guess_3, notes, time_state)')
			cur.execute('CREATE TABLE song_tags(tag, song_id)')
			cur.execute('CREATE TABLE anime_tags(tag, ann_id)')
			cur.execute('CREATE TABLE identified_songs(song_id_1, song_id_2)')
			cur.execute('CREATE TABLE blacklisted_songs(song_id)')
			cur.execute('CREATE TABLE database_updates(update_type, value, name)')
			cur.execute('CREATE TABLE anti_identified_songs(song_id_1, song_id_2)')
			con.commit()
		
		con.close()
	
	def get(self, *arr):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		ret = [cur.execute(f'SELECT * FROM {table}').fetchall() for table in arr]
		
		con.close()
		
		return ret[0] if len(ret) == 1 else ret
	
	def add_autotags(self, song, cur):
		ANIME_VINTAGE = True
		ANIME_YEAR =    True
		ANIME_TYPE = 	True
		SONG_CATEGORY = True
		SONG_TYPE = 	True
		
		anime_tags = cur.execute('SELECT * FROM anime_tags').fetchall()
		if ANIME_VINTAGE and song.vintage != None:
			tmp = '_'.join(song.vintage.split()[::-1])
			if (f'_vintage_{tmp}', song.ann_id) not in anime_tags:
				cur.execute('INSERT INTO anime_tags VALUES(?, ?)', (f'_vintage_{tmp}', song.ann_id))
		if ANIME_YEAR and song.vintage != None:
			if (f'_year_{song.vintage.split()[-1]}', song.ann_id) not in anime_tags:
				cur.execute('INSERT INTO anime_tags VALUES(?, ?)', (f'_year_{song.vintage.split()[-1]}', song.ann_id))
		if ANIME_TYPE and song.anime_type != None:
			tmp = '_'.join(song.anime_type.split())
			if (f'_type_{tmp}', song.ann_id) not in anime_tags:
				cur.execute('INSERT INTO anime_tags VALUES(?, ?)', (f'_type_{tmp}', song.ann_id))
		
		song_tags = cur.execute('SELECT * FROM song_tags').fetchall()
		if SONG_CATEGORY and song.category != None:
			tmp = '_'.join(song.category.split())
			if (f'_category_{tmp}', {song.song_id}) not in song_tags:
				cur.execute('INSERT INTO song_tags VALUES(?, ?)', (f'_category_{tmp}', song.song_id))
		if SONG_TYPE:
			if (f'_type_{song.song_type}', {song.song_id}) not in song_tags:
				cur.execute('INSERT INTO song_tags VALUES(?, ?)', (f'_type_{song.song_type}', song.song_id))
	
	def add_database_update(self, update_type, value, name):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		if (update_type, value, name) not in cur.execute('SELECT * FROM database_updates').fetchall():
			cur.execute('INSERT INTO database_updates VALUES(?, ?, ?)', (update_type, value, name))
		
		con.commit()
		con.close()
		
	
	def add_songs(self, songs, song_tags=[]):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for song in songs:
			a = cur.execute('SELECT * FROM songs WHERE song_id=?', (song.song_id,))
			if len(a.fetchall()) == 0:
				self.add_autotags(song, cur)
				cur.execute('INSERT INTO songs VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', (song.song_name, song.artist, song.anime, song.vintage, song.anime_type, song.category, song.song_type_specific, song.song_type, song.mp3, song.song_id, song.ann_id, song.state, song.guess_1, song.guess_2, song.guess_3, song.notes, song.time_state))
			for tag in song_tags:
				a = cur.execute('SELECT * FROM song_tags WHERE song_id=?', (song.song_id,))
				if (tag, song.song_id) not in a.fetchall():
					cur.execute('INSERT INTO song_tags VALUES(?, ?)', (tag, song.song_id))
		
		con.commit()
		con.close()
	
	def add_song_tags(self, arr):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		song_tags = cur.execute('SELECT * FROM song_tags').fetchall()
		for song_tag in arr:
			if song_tag in song_tags:
				continue
			tag, song_id = song_tag
			cur.execute('INSERT INTO song_tags VALUES(?, ?)', (tag, song_id))
		
		con.commit()
		con.close()
	
	def add_anime_tags(self, arr):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		anime_tags = cur.execute('SELECT * FROM anime_tags').fetchall()
		for anime_tag in arr:
			if anime_tag in anime_tags:
				continue
			tag, anime_id = anime_tag
			cur.execute('INSERT INTO anime_tags VALUES(?, ?)', (tag, anime_id))
		
		con.commit()
		con.close()
	
	def remove_song_tags(self, song_ids, tags):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for song_id in song_ids:
			for tag in tags:
				cur.execute('DELETE FROM song_tags WHERE song_id=? AND tag=?', (song_id, tag))
		
		con.commit()
		con.close()
	
	def untag_songs(self, song_ids):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for song_id in song_ids:
			cur.execute('DELETE FROM song_tags WHERE song_id=?', (song_id,))
		
		con.commit()
		con.close()
	
	def delete_song_tags(self, song_ids, tags):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for tag in tags:
			cur.execute('DELETE FROM song_tags WHERE tag=?', (tag,))
		
		con.commit()
		con.close()
	
	def remove_anime_tags(self, ann_ids, tags):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for ann_id in ann_ids:
			for tag in tags:
				cur.execute('DELETE FROM anime_tags WHERE ann_id=? AND tag=?', (ann_id, tag))
		
		con.commit()
		con.close()
	
	def untag_anime(self, ann_ids):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for ann_id in ann_ids:
			cur.execute('DELETE FROM anime_tags WHERE ann_id=?', (ann_id,))
		
		con.commit()
		con.close()
	
	def delete_anime_tags(self, tags):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for tag in tags:
			cur.execute('DELETE FROM anime_tags WHERE tag=?', (tag,))
		
		con.commit()
		con.close()
	
	def update_song(self, song):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		 
		cur.execute('DELETE FROM songs WHERE song_id=?', (song.song_id,))
		cur.execute('INSERT INTO songs VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)', (song.song_name, song.artist, song.anime, song.vintage, song.anime_type, song.category, song.song_type_specific, song.song_type, song.mp3, song.song_id, song.ann_id, song.state, song.guess_1, song.guess_2, song.guess_3, song.notes, song.time_state))
		
		con.commit()
		con.close()
	
	def identify_songs(self, song_ids):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		identified_songs = cur.execute('SELECT * FROM identified_songs').fetchall()
		song_ids = sorted(song_ids)
		for i in range(len(song_ids)):
			for j in range(i+1, len(song_ids)):
				if (song_ids[i], song_ids[j]) not in identified_songs:
					cur.execute('INSERT INTO identified_songs VALUES(?, ?)', (song_ids[i], song_ids[j]))
		
		con.commit()
		con.close()
	
	def unidentify_songs(self, song_ids):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for song_id in song_ids:
			cur.execute('DELETE FROM identified_songs WHERE song_id_1=? OR song_id_2=?', (song_id, song_id))
		
		con.commit()
		con.close()
	
	def anti_identify_songs(self, song_ids):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		anti_identified_songs = cur.execute('SELECT * FROM anti_identified_songs').fetchall()
		song_ids = sorted(song_ids)
		for i in range(len(song_ids)):
			for j in range(i+1, len(song_ids)):
				if (song_ids[i], song_ids[j]) not in anti_identified_songs:
					cur.execute('INSERT INTO anti_identified_songs VALUES(?, ?)', (song_ids[i], song_ids[j]))
		
		con.commit()
		con.close()
	
	def blacklist_songs(self, song_ids):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		blacklisted_songs = cur.execute('SELECT * FROM blacklisted_songs').fetchall()
		for song_id in song_ids:
			if song_id not in blacklisted_songs:
				cur.execute('INSERT INTO blacklisted_songs VALUES(?)', (song_id,))
		
		con.commit()
		con.close()
	
	def unblacklist_songs(self, song_ids):
		con = sqlite3.connect(self.DBFILE)
		cur = con.cursor()
		
		for song_id in song_ids:
			cur.execute('DELETE FROM blacklisted_songs WHERE song_id=?', (song_id,))
		
		con.commit()
		con.close()

class Anisongdb():
	def __init__(self):
		pass
	
	def _helper(song):
		song_name = song['songName']
		artist = song['songArtist']
		anime = song['animeJPName']
		vintage = song['animeVintage']
		anime_type = song['animeType']
		category = song['songCategory']
		song_type_specific = song['songType']
		song_type = song['songType'].split()[0]
		mp3 = song['audio']
		song_id = song['annSongId']
		ann_id = song['annId']
		
		return Song(song_name, artist, anime, vintage, anime_type, category, song_type_specific, song_type, mp3, song_id, ann_id)
	
	def get_artist(artist):
		tmp = {'and_logic': 'false', 'artist_search_filter': {'search': artist, 'partial_match': 'false', 'group_granularity': 0, 'max_other_artist': 99}, 'ignore_duplicate': 'false', 'opening_filter': 'true', 'ending_filter': 'true', 'insert_filter': 'true'}
		ret = requests.post('https://anisongdb.com/api/search_request', json = tmp)
		time.sleep(2)
		
		ids = []
		dct = defaultdict(list)
		for x in json.loads(ret.text):
			for artist_ in x['artists']:
				if artist == artist_['names'][0]:
					ids.append(artist_['id'])
					dct[artist_['id']].append(f'{x["songName"]} ({x["animeJPName"]})')
				if artist_['members'] != None:
					for member in artist_['members']:
						if artist == member['names'][0]:
							ids.append(member['id'])
							dct[member['id']].append(f'{x["songName"]} ({x["animeJPName"]})')
		ids = list(set(ids))
		if len(ids) > 1:
			print(f'Multiple people with name "{artist}" found:')
			for id_ in ids:
				a = str(id_) + ' '
				while len(a) < 8:
					a = ' ' + a
				print(f'id={a} {", ".join(list(set(dct[id_]))[:3])}')
			
			while True:
				a = input('Input id of desired person: ').strip()
				if a in list(map(str, ids)):
					ids = [int(a)]
					break
		
		i = 0
		songs = []
		song_ids = []
		while i != len(ids):
			artist_id = ids[i]
			
			tmp = {'artist_ids':[artist_id], 'group_granularity':0, 'max_other_artist':99, 'ignore_duplicate': 'false', 'opening_filter': 'true', 'ending_filter': 'true', 'insert_filter': 'true'}
			ret = requests.post('https://anisongdb.com/api/artist_ids_request', json = tmp)
			time.sleep(2)
			
			for x in json.loads(ret.text):
				if x['annSongId'] not in song_ids:
					song_ids.append(x['annSongId'])
					songs.append(x)
			
			i += 1
		
		return ids, [Anisongdb._helper(x) for x in songs]
	
	def get_composer(composer):
		tmp = {'and_logic': 'false', 'composer_search_filter': {'search': composer, 'partial_match': 'false', 'arrangement': 'true'}, 'ignore_duplicate': 'false', 'opening_filter': 'true', 'ending_filter': 'true', 'insert_filter': 'true'}
		ret = requests.post('https://anisongdb.com/api/search_request', json = tmp)
		time.sleep(2)
		
		ids = []
		dct = defaultdict(list)
		for x in json.loads(ret.text):
			for composer_ in x['composers']:
				if composer == composer_['names'][0]:
					ids.append(composer_['id'])
					dct[composer_['id']].append(f'{x["songName"]} ({x["animeJPName"]})')
				if composer_['members'] != None:
					for member in composer_['members']:
						if composer == member['names'][0]:
							ids.append(member['id'])
							dct[member['id']].append(f'{x["songName"]} ({x["animeJPName"]})')
		ids = list(set(ids))
		if len(ids) > 1:
			print(f'Multiple people with name "{composer}" found:')
			for id_ in ids:
				a = str(id_) + ' '
				while len(a) < 8:
					a = ' ' + a
				print(f'id={a} {", ".join(list(set(dct[id_]))[:3])}')
			
			while True:
				a = input('Input id of desired person: ').strip()
				if a in list(map(str, ids)):
					ids = [int(a)]
					break
		
		i = 0
		compositions = []
		arrangements = []
		song_ids = []
		while i != len(ids):
			composer_id = ids[i]
			
			tmp = {'composer_ids':[composer_id], 'arrangement': 'true', 'ignore_duplicate': 'false', 'opening_filter': 'true', 'ending_filter': 'true', 'insert_filter': 'true'}
			ret = requests.post('https://anisongdb.com/api/composer_ids_request', json = tmp)
			time.sleep(2)
			
			for x in json.loads(ret.text):
				if x['annSongId'] not in song_ids:
					song_ids.append(x['annSongId'])
					if any([composer_id == y['id'] for y in x['composers'] if x['composers'] != None]) or any([any([composer_id == z['id'] for z in y['members']]) for y in x['composers'] if x['composers'] != None and y['members'] != None]):
						compositions.append(x)
					if any([composer_id == y['id'] for y in x['arrangers'] if x['arrangers'] != None]) or any([any([composer_id == z['id'] for z in y['members']]) for y in x['arrangers'] if x['arrangers'] != None and y['members'] != None]):
						arrangements.append(x)
			
			i += 1
		
		return ids, [Anisongdb._helper(x) for x in compositions], [Anisongdb._helper(x) for x in arrangements]
	
	def get_ann_id(ann_id):
		tmp = {'annId': ann_id, 'ignore_duplicate': 'false', 'opening_filter': 'true', 'ending_filter': 'true', 'insert_filter': 'true'}
		ret = requests.post('https://anisongdb.com/api/annId_request', json = tmp)
		time.sleep(2)
		
		return [Anisongdb._helper(x) for x in json.loads(ret.text)]
	
	def get_artist_id(artist_id):
		songs = []
		
		tmp = {'artist_ids':[artist_id], 'group_granularity':0, 'max_other_artist':99, 'ignore_duplicate': 'false', 'opening_filter': 'true', 'ending_filter': 'true', 'insert_filter': 'true'}
		ret = requests.post('https://anisongdb.com/api/artist_ids_request', json = tmp)
		time.sleep(2)
		
		for x in json.loads(ret.text):
			songs.append(x)
		
		return [Anisongdb._helper(x) for x in songs]
	
	def get_composer_id(composer_id):
		compositions = []
		arrangements = []
			
		tmp = {'composer_ids':[composer_id], 'arrangement': 'true', 'ignore_duplicate': 'false', 'opening_filter': 'true', 'ending_filter': 'true', 'insert_filter': 'true'}
		ret = requests.post('https://anisongdb.com/api/composer_ids_request', json = tmp)
		time.sleep(2)
		
		for x in json.loads(ret.text):
			if any([composer_id == y['id'] for y in x['composers'] if x['composers'] != None]) or any([any([composer_id == z['id'] for z in y['members']]) for y in x['composers'] if x['composers'] != None and y['members'] != None]):
				compositions.append(x)
			if any([composer_id == y['id'] for y in x['arrangers'] if x['arrangers'] != None]) or any([any([composer_id == z['id'] for z in y['members']]) for y in x['arrangers'] if x['arrangers'] != None and y['members'] != None]):
				arrangements.append(x)
		
		return [Anisongdb._helper(x) for x in compositions], [Anisongdb._helper(x) for x in arrangements]
		

class Settings():
	def __init__(self, config_file='textfiles/default.conf'):
		if config_file != None:
			with open(config_file, 'r', encoding='UTF-8') as file:
				tmp = file.read().split('\n')
			
			self.guess_time = int(tmp[0])
			self.sample = int(tmp[1])
			self.full_song_range = int(tmp[2])
			self.song_selection = int(tmp[3])
			self.openings = int(tmp[4])
			self.endings = int(tmp[5])
			self.inserts = int(tmp[6])
			self.use_tags = int(tmp[7])
			self.song_tags = [x.split() for x in tmp[8].split('|')]
			self.anime_tags = [x.split() for x in tmp[9].split('|')]
	
	def setup(filename):
		def get_valid_input(prompt, is_valid):
			while True:
				s = input(prompt).strip()
				if is_valid(s):
					return s
		def helper(f, x):
			try:
				return f(x)
			except:
				return False
		with open(filename, 'w', encoding='UTF-8') as file:
			guess_time = get_valid_input('Guess time (positive integer): ', lambda x : helper(lambda X : int(X) > 0, x))
			sample = get_valid_input('Sample (0 - random, 1 - start, 2 - end): ', lambda x : helper(lambda X : int(X) in [0, 1, 2], x))
			full_song_range = get_valid_input('Full song range (0 - disabled, 1 - enabled): ', lambda x : helper(lambda X : int(X) in [0, 1], x))
			song_selection = get_valid_input('Song selection (0 - random, 1 - training mode, 2 - AMQ mode): ', lambda x : helper(lambda X : int(X) in [0, 1, 2], x))
			openings = get_valid_input('Opening songs (0 - disabled, 1 - enabled): ', lambda x : helper(lambda X : int(X) in [0, 1], x))
			endings = get_valid_input('Ending songs (0 - disabled, 1 - enabled): ', lambda x : helper(lambda X : int(X) in [0, 1], x))
			inserts = get_valid_input('Insert songs (0 - disabled, 1 - enabled): ', lambda x : helper(lambda X : int(X) in [0, 1], x))
			
			for line in [guess_time, sample, full_song_range, song_selection, openings, endings, inserts, 0, '' ,'']:
				print(line, file=file)
			
	
	def from_string(s):
		tmp = s.split(',')
		settings = Settings('textfiles/default.conf')
		if len(tmp[0]) > 0:
			settings.guess_time = int(tmp[0])
		if len(tmp[1]) > 0:
			settings.sample = int(tmp[1])
		if len(tmp[2]) > 0:
			settings.full_song_range = int(tmp[2])
		if len(tmp[3]) > 0:
			settings.song_selection = int(tmp[3])
		if len(tmp[4]) > 0:
			settings.openings = int(tmp[4])
		if len(tmp[5]) > 0:
			settings.endings = int(tmp[5])
		if len(tmp[6]) > 0:
			settings.inserts = int(tmp[6])
		if len(tmp[7]) > 0:
			settings.use_tags = int(tmp[7])
		settings.song_tags = [x.split() for x in tmp[8].split('|')]
		settings.anime_tags = [x.split() for x in tmp[9].split('|')]
		return settings
	
	def from_arr(arr):
		settings = Settings('./textfiles/default.conf')
		options = ['guess_time', 'sample', 'full_song_range', 'song_selection', 'openings', 'endings', 'inserts', 'use_tags', 'song_tags', 'anime_tags']
		tmp = [None] * len(options)
		for i in range(len(options)):
			for option in arr:
				if option[:1+len(options[i])] == options[i]+'=':
					if tmp[i] != None:
						print(f'Option "{options[i]}" appears multiple times in the given command.')
						exit()
					tmp[i] = option[1+len(options[i]):]
		
		if tmp[0] != None:
			settings.guess_time = int(tmp[0])
		if tmp[1] != None:
			settings.sample = int(tmp[1])
		if tmp[2] != None:
			settings.full_song_range = int(tmp[2])
		if tmp[3] != None:
			settings.song_selection = int(tmp[3])
		if tmp[4] != None:
			settings.openings = int(tmp[4])
		if tmp[5] != None:
			settings.endings = int(tmp[5])
		if tmp[6] != None:
			settings.inserts = int(tmp[6])
		if tmp[7] != None:
			settings.use_tags = int(tmp[7])
		if tmp[8] != None:
			settings.song_tags = [x.split() for x in tmp[8].split('|')]
		if tmp[9] != None:
			settings.anime_tags = [x.split() for x in tmp[9].split('|')]
		return settings

class Utility():
	def __init__(self):
		pass
		
	def _union_find(n, edges):
		parent = [i for i in range(n)]
		
		def p(u, parent):
			if parent[u] == u:
				return u
			parent[u] = p(parent[u], parent)
			return parent[u]
		
		for u, v in edges:
			if p(u, parent) == p(v, parent):
				continue
			if random.randint(0, 1):
				u, v = v, u
			parent[parent[u]] = v
		return [p(u, parent) for u in parent]
	
	def get_identified_songs(settings=Settings('textfiles/default.conf')):
		identified_songs = database.get('identified_songs')
		songs = Utility.get_songs(settings)
		song_ids = [song.song_id for song in songs]
		map_song_id_to_vertex = dict()
		map_vertex_to_song_id = []
		current_vertex = 0
		edges = []
		for x in song_ids:
			map_vertex_to_song_id.append(x)
			map_song_id_to_vertex[x] = current_vertex
			current_vertex += 1		
		for u, v in identified_songs:
			if u in song_ids and v in song_ids:
				edges.append((map_song_id_to_vertex[u], map_song_id_to_vertex[v]))
		parent = dict()
		_uf = Utility._union_find(len(song_ids), edges)
		for i in range(len(_uf)):
			parent[map_vertex_to_song_id[i]] = map_vertex_to_song_id[_uf[i]]
		children = defaultdict(list)
		for song_id in song_ids:
			children[parent[song_id]].append(song_id)
		return parent, children
	
	def get_blacklisted_songs():
		return [x[0] for x in database.get('blacklisted_songs')]
		
	def get_songs(settings=None):
		songs, song_tags, anime_tags, identified_songs = database.get('songs', 'song_tags', 'anime_tags', 'identified_songs')
		
		if settings == None:
			return [Song(*x) for x in songs]
		
		ann_ids = set([song[10] for song in songs])
		
		song_ids = set()
		for tagset in settings.song_tags:
			negatives = set([tag[1:] for tag in tagset if tag[0] == '-'])
			positives = set([tag for tag in tagset if tag[0] != '-'])
			
			tmp = defaultdict(int)
			do_not_include = set()
			for tag, song_id in song_tags:
				if tag in negatives:
					do_not_include.add(song_id)
				if tag in positives:
					tmp[song_id] += 1
			
			for song in songs:
				song_id = song[9]
				count = tmp[song_id]
				if count == len(positives) and song_id not in do_not_include:
					song_ids.add(song_id)
		
		anime_ids = set()
		for tagset in settings.anime_tags:
			negatives = set([tag[1:] for tag in tagset if tag[0] == '-'])
			positives = set([tag for tag in tagset if tag[0] != '-'])
			
			tmp = defaultdict(int)
			do_not_include = set()
			for tag, anime_id in anime_tags:
				if tag in negatives:
					do_not_include.add(anime_id)
				if tag in positives:
					tmp[anime_id] += 1
			
			for anime_id in ann_ids:
				count = tmp[anime_id]
				if count == len(positives) and anime_id not in do_not_include:
					anime_ids.add(anime_id)
		
		song_types = []
		if settings.openings:
			song_types.append('Opening')
		if settings.endings:
			song_types.append('Ending')
		if settings.inserts:
			song_types.append('Insert')
		
		blacklisted_songs = Utility.get_blacklisted_songs()
		
		if settings.use_tags == 0:
			return [Song(*x) for x in songs if x[7] in song_types and x[9] not in blacklisted_songs]
		elif settings.use_tags == 1:
			return [Song(*x) for x in songs if x[9] in song_ids and x[7] in song_types and x[9] not in blacklisted_songs]
		elif settings.use_tags == 2:
			return [Song(*x) for x in songs if x[10] in anime_ids and x[7] in song_types and x[9] not in blacklisted_songs]
		elif settings.use_tags == 3:
			return [Song(*x) for x in songs if (x[9] in song_ids or x[10] in anime_ids) and x[7] in song_types and x[9] not in blacklisted_songs]
		elif settings.use_tags == 4:
			return [Song(*x) for x in songs if x[9] in song_ids and x[10] in anime_ids and x[7] in song_types and x[9] not in blacklisted_songs]
		else:
			print(f'Invalid value {settings.use_tags} for use_tags setting. Defaulting to setting: ignoring tags.')
			return [Song(*x) for x in songs if x[7] in song_types and x[9] not in blacklisted_songs]
	
	def download_songs():
		songs = Utility.get_songs()
		blacklisted_songs = Utility.get_blacklisted_songs()
		flag = [0]
		threads = []
		for song in songs:
			if song.mp3 != None and song.song_id not in blacklisted_songs and song.mp3.split('/')[-1] not in os.listdir('songfiles'):
				flag[0] += 1
				def helper(mp3, songs, flag):
					try:
						wget.download(mp3, songs, lambda x, y, z: helper2(flag))
						flag[3] = time.time()
					except:
						flag[1] -= 1
					flag[0] -= 1
				def helper2(flag):
					print('\rDownloading songs.' + '.' * ((flag[2] // 300)%4) + ' ' * (3 - (flag[2] // 300)%4), end='')
					flag[2] += 1
				t = threading.Thread(target=helper, args=(song.mp3, 'songfiles', flag))
				threads.append(t)
		
		start_time = time.time()
		flag.append(flag[0])
		flag.append(0)
		flag.append(start_time)
		for thread in threads:
			thread.start()
		while flag[0]:
			time.sleep(1)
		print(f'\rDownloaded {flag[1]} file(s) in {int(1000 * (flag[3] - start_time)) / 1000}s.')
	
	def blacklist_songs(song_ids):
		database.blacklist_songs(list(map(int, song_ids)))
	
	def unblacklist_songs(song_ids):
		database.unblacklist_songs(list(map(int, song_ids)))
	
	def purge_songs():
		songs = Utility.get_songs()
		blacklisted_songs = Utility.get_blacklisted_songs()
		mp3s = set([song.mp3.split('/')[-1] for song in songs if song.mp3 != None])
		blacklist = set([song.mp3.split('/')[-1] for song in songs if song.mp3 != None and song.song_id in blacklisted_songs])
		
		for file in os.listdir('songfiles'):
			if file not in mp3s or file in blacklist:
				os.remove(f'songfiles/{file}')
	
	def update_song_info():
		database_updates = database.get('database_updates')
		database_songs = Utility.get_songs()
		map_song_id_to_song = dict()
		for song in database_songs:
			map_song_id_to_song[song.song_id] = song
		
		songs = []
		for update_type, value, name in database_updates:
			if update_type == 'artist':
				songs += Anisongdb.get_artist_id(value)
			elif update_type == 'composer':
				compositions, arrangements = Anisongdb.get_composer_id(value)
				songs += compositions + arrangements
			elif update_type == 'anime':
				songs += Anisongdb.get_ann_id(value)
		
		for song in songs:
			db_song = map_song_id_to_song[song.song_id]
			
			db_song.song_name = song.song_name
			db_song.artist = song.artist
			db_song.anime = song.anime
			db_song.vintage = song.vintage
			db_song.anime_type = song.anime_type
			db_song.category = song.category
			db_song.song_type_specific = song.song_type_specific
			db_song.song_type = song.song_type
			db_song.mp3 = song.mp3
			db_song.song_id = song.song_id
			db_song.ann_id = song.ann_id
		
		for song in database_songs:
			database.update_song(song)
	
	def add_ann_ids(ann_ids):
		songs = []
		for ann_id in ann_ids:
			tmp = Anisongdb.get_ann_id(ann_id)
			if len(tmp) > 0:
				songs += tmp
				database.add_database_update('anime', ann_id, tmp[-1].anime)
		
		database.add_songs(songs)
		Utility.download_songs()
	
	def add_artist(artists):
		for artist in artists:
			ids, songs = Anisongdb.get_artist(artist.strip())
			database.add_songs(songs, ['_'.join(['artist'] + artist.replace(',', '').replace('|', '').split())])
			for id_ in ids:
				database.add_database_update('artist', id_, artist)
		Utility.download_songs()
	
	def add_composer(composers):
		for composer in composers:
			ids, compositions, arrangements = Anisongdb.get_composer(composer.strip())
			database.add_songs(compositions, ['_'.join(['composer'] + composer.split())])
			database.add_songs(arrangements, ['_'.join(['arranger'] + composer.split())])
			for id_ in ids:
				database.add_database_update('composer', id_, composer)
		Utility.download_songs()
	
	def add_song_tags(song_ids_tags):
		try:
			song_tags = []
			song_ids = [x.strip() for x in song_ids_tags if len(x) > 0 and x.strip()[0] in '0123456789']
			tags = [x.strip() for x in song_ids_tags if len(x) > 0 and x.strip()[0] not in '0123456789']
			assert(all([x[0] != '0' for x in song_ids]) and all([x[0] != '-' for x in tags]))
			assert(all([all([digit in '0123456789' for digit in song_id]) for song_id in song_ids]))
			assert(all([all([character not in '=,|' for character in tag]) for tag in tags]))
			for tag in tags:
				for song_id in song_ids:
					song_tags.append((tag, int(song_id)))
			song_tags = list(set(song_tags))
			database.add_song_tags(song_tags)
		except:
			print('Invalid input: tag must not contain a "|", a "=", or a "," and must not start with a "-" or a digit, and song_id must be a positive integer with no leading zeros.')
	
	def add_anime_tags(ann_ids_tags):
		try:
			anime_tags = []
			ann_ids = [x.strip() for x in ann_ids_tags if len(x) > 0 and x.strip()[0] in '0123456789']
			tags = [x.strip() for x in ann_ids_tags if len(x) > 0 and x.strip()[0] not in '0123456789']
			assert(all([x[0] != '0' for x in ann_ids]) and all([x[0] != '-' for x in tags]))
			assert(all([all([digit in '0123456789' for digit in ann_id]) for ann_id in ann_ids]))
			assert(all([all([character not in '=,|' for character in tag]) for tag in tags]))
			for tag in tags:
				for ann_id in ann_ids:
					anime_tags.append((tag, int(ann_id)))
			anime_tags = list(set(anime_tags))
			database.add_anime_tags(anime_tags)
		except:
			print('Invalid input: tag must not contain a "|", a "=", or a "," and must not start with a "-" or a digit, and ann_id must be a positive integer with no leading zeros.')
	
	def remove_song_tags(arr):
		try:
			tags = [x for x in arr if x[0] not in '0123456789']
			song_ids = [int(x) for x in arr if x[0] in '0123456789']
			assert(all([x[0] != '-' for x in tags]))
			assert(all([all([digit in '0123456789' for digit in song_id]) for song_id in song_ids]))
			assert(all([all([character not in '=,|' for character in tag]) for tag in tags]))
			
			database.remove_song_tags(song_ids, tags)
		except:
			print('Invalid input: tag must not contain a "|", a "=", or a "," and must not start with a "-" or a digit, and song_id must be a positive integer with no leading zeros.')
	
	def untag_songs(arr):
		try:
			assert(all([all([digit in '0123456789' for digit in id_]) for id_ in arr]))
			song_ids = [int(x) for x in arr]
			
			database.untag_songs(song_ids)
		except:
			print('Invalid input: song_id must be a positive integer with no leading zeros.')
	
	def delete_song_tags(tags):
		try:
			assert(all([x[0] not in '-0123456789' for x in tags]))
			assert(all([all([character not in '=,|' for character in tag]) for tag in tags]))
			
			database.delete_song_tags(tags)
		except:
			print('Invalid input: tag must not contain a "|", a "=", or a "," and must not start with a "-" or a digit.')
	
	def remove_anime_tags(arr):
		try:
			tags = [x for x in arr if x[0] not in '0123456789']
			ann_ids = [int(x) for x in arr if x[0] in '0123456789']
			assert(all([x[0] != '-' for x in tags]))
			assert(all([all([digit in '0123456789' for digit in ann_id]) for an_id in ann_ids]))
			assert(all([all([character not in '=,|' for character in tag]) for tag in tags]))
			
			database.remove_anime_tags(ann_ids, tags)
		except:
			print('Invalid input: tag must not contain a "|", a "=", or a "," and must not start with a "-" or a digit, and ann_id must be a positive integer with no leading zeros.')
	
	def untag_anime(arr):
		try:
			assert(all([all([digit in '0123456789' for digit in id_]) for id_ in arr]))
			ann_ids = [int(x) for x in arr]
			
			database.untag_anime(ann_ids)
		except:
			print('Invalid input: ann_id must be a positive integer with no leading zeros.')
	
	def delete_anime_tags(tags):
		try:
			assert(all([x[0] not in '-0123456789' for x in tags]))
			assert(all([all([character not in '=,|' for character in tag]) for tag in tags]))
			
			database.delete_anime_tags(tags)
		except:
			print('Invalid input: tag must not contain a "|", a "=", or a "," and must not start with a "-" or a digit.')
	
	def list_tags():
		song_tags, anime_tags = database.get('song_tags', 'anime_tags')
		print( f'Song tags: {", ".join(sorted(set([x[0] for x in  song_tags]), key = lambda a:(a[0] == "_", a)))}')
		print(f'Anime tags: {", ".join(sorted(set([x[0] for x in anime_tags]), key = lambda a:(a[0] == "_", a)))}')
	
	def add_song_notes(song_notes):
		songs = database.get('songs')
		songs = [Song(*x) for x in songs]
		
		song_notes_list = []
		tmp = defaultdict(int)
		
		song_notes_list = [(int(song_notes[0]), ' '.join(song_notes[1:]))]
		tmp[int(song_notes[0])] = 1
		
		for song_id, notes in song_notes_list:
			try:
				song = [song for song in songs if song.song_id == song_id][0]
			except:
				print(f'No song in the database with song_id={song_id}.')
				continue
			if len(song.notes.strip()) == 0:
				song.notes = notes
				database.update_song(song)
			else:
				while True:
					overwrite = input(f'Song with song_id={song_id} has existing notes:\n{song.notes}\nNew notes:\n{notes}\nOverwrite old notes? (y/n) ').lower().strip()
					if overwrite in ['y' , 'n', 'yes', 'no']:
						break
				if overwrite in ['y', 'yes']:
					song.notes = notes
					database.update_song(song)
	
	def identify_songs(song_ids):
		song_id_list = [song[9] for song in database.get('songs')]
		arr = []
		for song_id in song_ids:
			if int(song_id) not in song_id_list:
				print(f'No song in the database with song_id={song_id}. Ignoring identification of this song_id.')
			else:
				arr.append(int(song_id))
		database.identify_songs(arr)
	
	def unidentify_songs(song_ids):
		database.unidentify_songs(list(map(int, song_ids)))
	
	def auto_identify():
		songs = Utility.get_songs()
		parent, children = Utility.get_identified_songs()
		anti_identified_songs = database.get('anti_identified_songs')
		dct = defaultdict(list)
		
		for song in songs:
			dct[song.song_name].append((song.artist, song.song_id))
		
		edges = []
		anti_edges = []
		for song_name in dct:
			done = []
			for artist, id_ in dct[song_name]:
				if id_ in done:
					continue
				done.append(id_)
				for artist_, id__ in dct[song_name]:
					if id_ in parent and id__ in parent and parent[id_] == parent[id__]:
						continue
					if id__ in done:
						continue
					if artist == artist_:
						edges.append((id_, id__))
						done.append(id__)
					elif (id_, id__) not in anti_identified_songs and (id__, id_) not in anti_identified_songs:
						def _helper(s, n):
							c = os.get_terminal_size().columns - 1
							if c <= n:
								return s
							
							arr = s.split('\n')
							ret = []
							for line in arr:
								line = line.split('\r')[-1]
								if len(line) == 0:
									ret.append(line)
								while len(line) > c:
									ret.append(line[:c])
									line = ' ' * n + line[c:].strip()
								if len(line) > 0:
									ret.append(line)
							
							return '\n'.join(ret)
						print('Found two songs with matching song names:')
						print(_helper(f'    Song Name: {song_name}', len('    Song Name: ') + 2))
						print(_helper(f'Song 1 Artist: {artist}', len('    Song Name: ') + 2))
						print(_helper(f'Song 2 Artist: {artist_}', len('    Song Name: ') + 2))
						while True:
							a = input('Identify these songs? (y/n) ').strip().lower()
							if a in ['y', 'n', 'yes', 'no']:
								print()
								break
						if a in ['y', 'yes']:
							edges.append((id_, id__))
							done.append(id__)
						else:
							anti_edges.append((id_, id__))
		for edge in edges:
			database.identify_songs(edge)
		parent, children = Utility.get_identified_songs()
		for (u, v) in anti_edges:
			for u in children[parent[u]]:
				for v in children[parent[v]]:
					database.anti_identify_songs((u, v))

	def dump():
		songs, song_tags, anime_tags, identified_songs, blacklisted_songs, database_updates = database.get('songs', 'song_tags', 'anime_tags', 'identified_songs', 'blacklisted_songs', 'database_updates')
		
		with open(f'textfiles/dump_songs{"_" + database.DBFILE if database.DBFILE != "db.db" else ""}.txt', 'w', encoding='UTF-8') as file:
			for line in songs:
				print('\t'.join(list(map(str, line))), file=file)
		
		with open(f'textfiles/dump_song_tags{"_" + database.DBFILE if database.DBFILE != "db.db" else ""}.txt', 'w', encoding='UTF-8') as file:
			tmp = defaultdict(list)
			for tag, song_id in song_tags:
				tmp[song_id].append(tag)
			for song_id in tmp:
				print('\t'.join(list(map(str, [song_id] + [*sorted(tmp[song_id], key = lambda a : (a[0] == '_', a))]))), file=file)
		
		with open(f'textfiles/dump_anime_tags{"_" + database.DBFILE if database.DBFILE != "db.db" else ""}.txt', 'w', encoding='UTF-8') as file:
			tmp = defaultdict(list)
			for tag, ann_id in anime_tags:
				tmp[ann_id].append(tag)
			for ann_id in tmp:
				print('\t'.join(list(map(str, [ann_id] + [*sorted(tmp[ann_id], key = lambda a : (a[0] == '_', a))]))), file=file)
		
		with open(f'textfiles/dump_identified_songs{"_" + database.DBFILE if database.DBFILE != "db.db" else ""}.txt', 'w', encoding='UTF-8') as file:
			parent, children = Utility.get_identified_songs()
			for line in children:
				print('\t'.join(list(map(str, children[line]))), file=file)
		
		with open(f'textfiles/dump_blacklisted_songs{"_" + database.DBFILE if database.DBFILE != "db.db" else ""}.txt', 'w', encoding='UTF-8') as file:
			for line in blacklisted_songs:
				print('\t'.join(list(map(str, line))), file=file)
		
		with open(f'textfiles/dump_database_updates{"_" + database.DBFILE if database.DBFILE != "db.db" else ""}.txt', 'w', encoding='UTF-8') as file:
			for line in database_updates:
				print('\t'.join(list(map(str, line))), file=file)
	
	def append_song_notes(song):
		notes = input('Enter notes: ').strip()
		if len(song.notes) == 0:
			song.notes = notes
		else:
			song.notes = (f'{song.notes} {notes}').strip()
		database.update_song(song)
	
	def is_valid_tagset(tags):
		try:
			assert(all([x[0] not in '-0123456789' for x in tags]))
			assert(all([all([character not in '=,|' for character in tag]) for tag in tags]))
		except:
			print('Invalid input: tag must not contain a "|", a "=", or a "," and must not start with a "-" or a digit.')
			return False
		return True

class Player():
	def __init__(self, settings):
		import pygame
		pygame.init()
		
		self.music = pygame.mixer.music
		self.settings = settings
		self.song_directory = 'songfiles'

	def _play_song(self, mp3, sample, flag):
		self.music.load(mp3)
		self.music.set_volume(0)
		self.music.play()
		self.music.pause()
		self.music.rewind()
		self.music.set_pos(sample)
		self.music.set_volume(1)
		start_time = time.time()
		flag.append(start_time)
		self.music.unpause()
		
		while time.time() - start_time < self.settings.guess_time and flag[0]:
			pass
		self.music.pause()
		while flag[0]:
			pass
		end_time = time.time()
		flag.append(end_time - start_time)
		self.music.unload()
	
	def play_song(self):
		songs = Utility.get_songs(self.settings)
		map_song_id_to_song = dict()
		for song in songs:
			map_song_id_to_song[song.song_id] = song
		parent, children = Utility.get_identified_songs(self.settings)
		
		if len(songs) == 0:
			print('\nNo songs under these settings.')
			exit()
		
		if self.settings.song_selection == 1:
			songs = [song for song in songs if song.song_id not in parent or parent[song.song_id]==song.song_id]
			
			if random.randint(0, 1):
				state = dict()
				for song_id in children:
					state[song_id] = map_song_id_to_song[song_id].state
					for child in children[song_id]:
						state[song_id] = min(state[song_id], map_song_id_to_song[child].state)
				
				m = min([song.state for song in songs] + [state[song_id] for song_id in state])
				arr = [(song, 2**(state[song.song_id]-m)) if song.song_id in parent else (song, 2**(song.state-m)) for song in songs]
				total = sum([y for (x, y) in arr])
				i = random.randint(0, total-1)
				curr = 0
				for _song, weight in arr:
					curr += weight
					if i < curr:
						if _song.song_id not in parent:
							song = _song
						else:
							song = random.choice([map_song_id_to_song[song_id] for song_id in children[_song.song_id]])
						break
			else:
				def w(t0):
					t = int(time.time())
					return (t - t0) // (60 * 60 * 24 * 7)
				
				state = dict()
				for song_id in children:
					state[song_id] = map_song_id_to_song[song_id].state + w(map_song_id_to_song[song_id].time_state)
					for child in children[song_id]:
						state[song_id] = min(state[song_id], map_song_id_to_song[child].state + w(map_song_id_to_song[child].time_state))
				
				m = min([song.state + w(song.time_state) for song in songs] + [state[song_id] for song_id in state])
				arr = [(song, 2**(state[song.song_id]-m)) if song.song_id in parent else (song, 2**(song.state+w(song.time_state)-m)) for song in songs]
				total = sum([y for (x, y) in arr])
				i = random.randint(0, total-1)
				curr = 0
				for _song, weight in arr:
					curr += weight
					if i < curr:
						if _song.song_id not in parent:
							song = _song
						else:
							song = random.choice([map_song_id_to_song[song_id] for song_id in children[_song.song_id]])
						break
		elif self.settings.song_selection == 2:
			tmp = defaultdict(list)
			for song in songs:
				tmp[song.ann_id].append(song)
			arr = []
			total = 0
			for ann_id in tmp:
				total += (3+len(tmp[ann_id]))//4
				arr.append((ann_id, (3+len(tmp[ann_id]))//4))
			i = random.randint(0, total-1)
			curr = 0
			for ann_id, weight in arr:
				curr += weight
				if i < curr:
					song = random.choice(tmp[ann_id])
					break
		elif self.settings.song_selection == 0:
			songs = [song for song in songs if song.song_id not in parent or parent[song.song_id]==song.song_id]
			
			_song = random.choice(songs)
			
			if _song.song_id not in parent:
				song = _song
			else:
				song = random.choice([map_song_id_to_song[song_id] for song_id in children[_song.song_id]])
		else:
			print(f'Invalid value "{self.settings.song_selection}" for song_selection setting. Defaulting to setting: equal probability.')
			songs = [song for song in songs if song.song_id not in parent or parent[song.song_id]==song.song_id]
			
			_song = random.choice(songs)
			
			if song.song_id not in parent:
				_song = _song
			else:
				song = random.choice([map_song_id_to_song[song_id] for song_id in children[_song.song_id]])
		
		try:
			mp3 = f'{self.song_directory}/{song.mp3.split("/")[-1]}'
			length = mutagen.mp3.MP3(mp3).info.length
		except:
			print('\nFile not found.\n')
		
			print(  f"""      Anime: {song.anime}
		              \r  Song Name: {song.song_name}
		              \r     Artist: {song.artist}
		              \r  Song Type: {song.song_type_specific}
	                  \r    Song ID: {song.song_id}\n""")
			
			return None, None
		
		if self.settings.sample == 0:
			sample = random.randint(0, max(0, int(length)-self.settings.guess_time-10*(1^self.settings.full_song_range)))
		elif self.settings.sample == 1:
			sample = 0
		elif self.settings.sample == 2:
			sample = max(0, int(length)-self.settings.guess_time-10*(1^self.settings.full_song_range))
		else:
			print(f'Invalid value "{self.settings.sample}" for sample setting. Defaulting to setting: random sample.')
			self.settings.sample = 0
			sample = random.randint(0, max(0, int(length)-self.settings.guess_time-10*(1^self.settings.full_song_range)))
		
		flag = [1]
		t = threading.Thread(target=self._play_song, args=(mp3, sample, flag))
		tt = threading.Thread(target=self._play_song, args=(mp3, sample, flag))
		print()
		t.start()
		g = input(' Your Guess: ')
		flag[0] = 0
		time.sleep(0.2)
		flag[0] = 1
		tt.start()
		
		guess_time = flag[-1]
		exponent = 0
		while guess_time >= 10**exponent:
			exponent += 1
		guess_time = str(guess_time)[:max(5, exponent+4)] + 's'
		
		def _helper(s, n):
			c = os.get_terminal_size().columns - 1
			if c <= n:
				return s
			
			arr = s.split('\n')
			ret = []
			for line in arr:
				line = line.split('\r')[-1]
				if len(line) == 0:
					ret.append(line)
				while len(line) > c:
					ret.append(line[:c])
					line = ' ' * n + line[c:].strip()
				if len(line) > 0:
					ret.append(line)
			
			return '\n'.join(ret)
		
		def sec_to_minsec(sec):
			m = sec // 60
			sec %= 60
			return f'{m}:{"0" if sec < 10 else ""}{sec}'
		
		alt_answers = list(set([map_song_id_to_song[song_id].anime for song_id in children[parent[song.song_id]] if map_song_id_to_song[song_id].anime != song.anime and map_song_id_to_song[song_id].artist == song.artist]))
		if len(alt_answers) > 0:
			tmp = '\n             '.join(alt_answers)
			s = f""" Guess Time: {guess_time}\n
		          \r      Anime: {song.anime}
		          \rOther Anime: {tmp}
		          \r  Song Name: {song.song_name}
		          \r     Artist: {song.artist}
		          \r  Song Type: {song.song_type_specific}
		          \r     Sample: {sec_to_minsec(sample)}/{sec_to_minsec(int(length))}
		          \r    Song ID: {song.song_id}"""
		else:
			s = f""" Guess Time: {guess_time}\n
		          \r      Anime: {song.anime}
		          \r  Song Name: {song.song_name}
		          \r     Artist: {song.artist}
		          \r  Song Type: {song.song_type_specific}
		          \r     Sample: {sec_to_minsec(sample)}/{sec_to_minsec(int(length))}
		          \r    Song ID: {song.song_id}"""
		if len(song.notes) > 0:
			s += f'\n      Notes: {song.notes}'
		print(_helper(s, len(' Your Guess: ') + 2))
		print()
		
		if self.settings.song_selection == 1:
			if song.song_id in parent:
				history = []
				if len(song.guess_2) > 0: 
					song.guess_3 = f'{g} ({guess_time})'
				elif len(song.guess_1) > 0:
					song.guess_2 = f'{g} ({guess_time})'
				else:
					song.guess_1 = f'{g} ({guess_time})'
				
				for id_ in children[parent[song.song_id]]:
					_song = map_song_id_to_song[id_]
					if len(_song.guess_1) > 0:
						history.append(_song.guess_1)
					if len(_song.guess_2) > 0:
						history.append(_song.guess_2)
					if len(_song.guess_3) > 0:
						history.append(_song.guess_3)
				
				s = f'    History: {", ".join(history)}\n'
				print(_helper(s, len(' Guess Time: ') + 2))
				
				if len(history) >= 3:
					while True:
						a = input('Change frequency? (-1 = less frequent, 0 = keep frequency, 1 = more frequent): ')
						try:
							a = int(a)
							assert(a in [-1, 0, 1])
							print()
							break
						except:
							pass
					for id_ in children[parent[song.song_id]]:
						_song = map_song_id_to_song[id_]
						_song.guess_1 = ''
						_song.guess_2 = ''
						_song.guess_3 = ''
						_song.state += a
				
				for id_ in children[parent[song.song_id]]:
					_song = map_song_id_to_song[id_]
					_song.time_state = _song.time_state + int(time.time()) >> 1
					database.update_song(_song)
					
			else:
				history = []
				if len(song.guess_2) > 0: 
					song.guess_3 = f'{g} ({guess_time})'
				elif len(song.guess_1) > 0:
					song.guess_2 = f'{g} ({guess_time})'
				else:
					song.guess_1 = f'{g} ({guess_time})'
				
				if len(song.guess_1) > 0:
					history.append(song.guess_1)
				if len(song.guess_2) > 0:
					history.append(song.guess_2)
				if len(song.guess_3) > 0:
					history.append(song.guess_3)
				
				s = f'    History: {", ".join(history)}\n'
				print(_helper(s, len(' Guess Time: ') + 2))
				
				if len(song.guess_3) > 0:
					song.guess_1 = ''
					song.guess_2 = ''
					song.guess_3 = ''
					while True:
						a = input('Change frequency? (-1 = less frequent, 0 = keep frequency, 1 = more frequent): ')
						try:
							a = int(a)
							assert(a in [-1, 0, 1])
							break
						except:
							pass
					song.state += a
				
				song.time_state = song.time_state + int(time.time()) >> 1
				database.update_song(song)
		
		return song, flag

	def play(self):
		while True:
			time.sleep(0.1)
			song, flag = self.play_song()
			print('Available commands: exit, blacklist_song, add_notes, add_song_tags, add_anime_tags.')
			s = input('Input a command or press <Enter> to continue. ').strip().lower()
			if s == 'exit':
				if flag != None:
					flag[0] = 0
				exit()
			elif s == 'add_notes':
				Utility.append_song_notes(song)
			elif s == 'blacklist_song':
				Utility.blacklist_songs([str(song.song_id)])
				print('Added song to blacklist.')
			elif s == 'add_song_tags':
				while True:
					tags = input('Input tag(s): ').split()
					if Utility.is_valid_tagset(tags):
						Utility.add_song_tags([str(song.song_id)] + tags)
						print(f'Added tag(s) {tags} to song.')
						break
			elif s == 'add_anime_tags':
				while True:
					tags = input('Input tag(s): ').split()
					if Utility.is_valid_tagset(tags):
						Utility.add_anime_tags([str(song.ann_id)] + tags)
						print(f'Added tag(s) {tags} to anime.')
						break
			if flag != None:
				flag[0] = 0

def main():
	def _helper(s, n):
		c = os.get_terminal_size().columns - 1
		if c <= n:
			return s
		
		arr = s.split('\n')
		ret = []
		for line in arr:
			line = line.split('\r')[-1]
			if len(line) == 0:
				ret.append(line)
			while len(line) > c:
				ret.append(line[:c])
				line = ' ' * n + line[c:].strip()
			if len(line) > 0:
				ret.append(line)
		
		return '\n'.join(ret)
	
	HELP_MESSAGE = """Usage: python main.py <command> [options]

python main.py  play [config_file|raw_config|config_options]      Starts the training tool using the given configuration.
                add_ann_ids [ann_ids...]                          Adds the songs of the given ann_ids to the database and downloads songs.
                add_artists [artists...]                          Adds artists to the database and downloads songs.
                add_composers [composers...]                      Adds composers to the database and downloads songs.
                add_song_tags [song_ids... song_tags...]          Adds tags to songs. Tags must not start with a '-' or a digit.
                add_anime_tags [ann_ids... anime_tags...]         Adds tags to anime. Tags must not start with a '-' or a digit.
                remove_song_tags [song_ids... song_tags...]       Removes the tags from the songs.
                untag_songs [song_ids...]                         Removes all tags from the songs.
                delete_song_tags [song_tags...]                   Removes the tags from all songs.
                remove_anime_tags [ann_ids... anime_tags...]      Removes the tags from the anime.
                untag_anime [ann_ids...]                          Removes all tags from the anime.
                delete_anime_tags [anime_tags...]                 Removes the tags from all anime.
                add_song_notes [song_id notes]                    Adds notes to a song.
                identify_songs [song_ids...]                      Identifies songs together, preventing them from spamming in random and training mode.
                unidentify_songs [song_ids...]                    For each song, removes all identification relationships involving that song.
                auto_identify                                     Identifies songs with matching song names and artists, and prompts songs with matching song names.
                download_songs                                    Downloads songs in the database that are missing in the songfiles folder.
                blacklist_songs [song_ids...]                     Blacklists songs so they don't play and/or get downloaded.
                unblacklist_songs [song_ids...]                   Removes songs from the blacklist.
                purge_songs                                       Deletes songfiles that do not have a corresponding song in the database.
                update_song_info                                  Updates song info of all songs in the database.
                list_tags                                         Prints all existing song tags and anime tags.
                output_database                                   Prints the contents of the database into files in the textfiles folder.
                help                                              Prints this message.

To see some sample commands, go to textfiles/sample_commands.txt."""
	
	if len(sys.argv) == 1:
		print(_helper(HELP_MESSAGE, 70))
		return
	
	global database
	if sys.argv[1][:3] == 'db=':
		database = Database(sys.argv[1][3:])
		del sys.argv[1]
	else:
		database = Database()
	
	commands = ['play', 'add_ann_id', 'add_annid', 'add_ann_ids', 'add_annids', 'add_artist', 'add_artists', 'add_composer', 'add_composers', 'add_song_tags', 'add_anime_tags', 'remove_song_tags', 'untag_songs', 'delete_song_tags', 'remove_anime_tags', 'untag_anime', 'delete_anime_tags', 'add_song_notes', 'identify_songs', 'unidentify_songs', 'auto_identify', 'download_songs', 'blacklist_songs', 'unblacklist_songs', 'purge_songs', 'update_song_info', 'list_tags', 'output_database', 'help']
	if sys.argv[1] not in commands:
		print(f'Command "{sys.argv[1]}" does not exist.')
		print(_helper(HELP_MESSAGE, 70))
		return
	
	elif sys.argv[1] == 'help':
		print(_helper(HELP_MESSAGE, 70))
		return
	
	elif sys.argv[1] == 'play':
		if len(sys.argv) == 2:
			if not os.path.isfile('config.conf'):
				while True:
					s = input('Settings have not been configured. Proceed with default settings? (y/n) ').strip().lower()
					if s in ['y', 'n', 'yes', 'no']:
						break
				if s in ['y', 'yes']:
					Player(Settings()).play()
				else:
					Settings.setup('config.conf')
					Player(Settings('config.conf')).play()
			else:
				Player(Settings('config.conf')).play()
		elif os.path.isfile(sys.argv[2]):
			Player(Settings(sys.argv[2])).play()
		elif '=' in sys.argv[2]:
			Player(Settings.from_arr(sys.argv[2:])).play()
		else:
			Player(Settings.from_string(sys.argv[2])).play()
	
	elif sys.argv[1] in ['add_ann_ids', 'add_annids', 'add_ann_id', 'add_annid']:
		Utility.add_ann_ids(sys.argv[2:])
	
	elif sys.argv[1] in ['add_artists', 'add_artist']:
		Utility.add_artist(sys.argv[2:])
	
	elif sys.argv[1] in ['add_composers', 'add_composer']:
		Utility.add_composer(sys.argv[2:])
	
	elif sys.argv[1] == 'add_song_tags':
		Utility.add_song_tags(sys.argv[2:])
	
	elif sys.argv[1] == 'add_anime_tags':
		Utility.add_anime_tags(sys.argv[2:])
	
	elif sys.argv[1] == 'remove_song_tags':
		Utility.remove_song_tags(sys.argv[2:])
	
	elif sys.argv[1] == 'untag_songs':
		Utility.untag_songs(sys.argv[2:])
	
	elif sys.argv[1] == 'delete_song_tags':
		Utility.delete_song_tags(sys.argv[2:])
	
	elif sys.argv[1] == 'remove_anime_tags':
		Utility.remove_anime_tags(sys.argv[2:])
	
	elif sys.argv[1] == 'untag_anime':
		Utility.untag_anime(sys.argv[2:])
	
	elif sys.argv[1] == 'delete_anime_tags':
		Utility.delete_anime_tags(sys.argv[2:])
	
	elif sys.argv[1] == 'add_song_notes':
		Utility.add_song_notes(sys.argv[2:])
	
	elif sys.argv[1] == 'identify_songs':
		Utility.identify_songs(sys.argv[2:])
	
	elif sys.argv[1] == 'unidentify_songs':
		Utility.unidentify_songs(sys.argv[2:])
	
	elif sys.argv[1] == 'auto_identify':
		Utility.auto_identify()
	
	elif sys.argv[1] == 'download_songs':
		Utility.download_songs()
	
	elif sys.argv[1] == 'blacklist_songs':
		Utility.blacklist_songs(sys.argv[2:])
	
	elif sys.argv[1] == 'unblacklist_songs':
		Utility.unblacklist_songs(sys.argv[2:])
	
	elif sys.argv[1] == 'purge_songs':
		Utility.purge_songs()
	
	elif sys.argv[1] == 'update_song_info':
		Utility.update_song_info()
	
	elif sys.argv[1] == 'list_tags':
		Utility.list_tags()
	
	elif sys.argv[1] == 'output_database':
		Utility.dump()

if __name__=='__main__':
	main()