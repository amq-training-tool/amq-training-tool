Add Strike Witches: Road to Berlin to the database, add the tag "anime_Road_to_Berlin" to the anime, identify the ending songs, and play.

	python main.py add_annid 21189
	python main.py add_anime_tags 21189 anime_Road_to_Berlin
	python main.py auto_identify
	y
	y
	y
	y
	y
	y
	y
	y
	y
	y
	y
	python main.py play guess_time=20 sample=0 full_song_range=0 song_selection=0 openings=1 endings=1 inserts=1 use_tags=2 song_tags= anime_tags=anime_Road_to_Berlin
	

Add yozuca* and Minami Kuribayashi as composermania options, and play songs composed or arranged by them.

	python main.py add_composers "Minami Kuribayashi" yozuca*
	python main.py auto_identify
	python main.py play "20,0,0,0,1,1,1,1,composer_Minami_Kuribayashi|arranger_Minami_Kuribayashi|composer_yozuca*|arranger_yozuca*,"
	

Add Nao Touyama and Saori Hayami as artistmania options, choose the correct Nao Touyama, and play only songs where they both sing.

	python main.py add_artists "Nao Touyama" "Saori Hayami"
	5072
	python main.py auto_identify
	...
	python main.py play use_tags=1 "song_tags=artist_Nao_Touyama artist_Saori_Hayami"
	

Add every Inital D entry, and play insert songs only with song selection set to training mode.

	python main.py add_annids 395 1152 1153 1154 1155 3396 7986 10222 14572 15711 16073 16821 17511 24123 25314
	python main.py auto_identify
	python main.py play ,,,1,0,0,1,,,
	
